import React, { useState, useEffect } from "react";
import Europa from './Europa'
import AmericaNorte from './AmericaNorte'
import AmericaSur from './AmericaSur'
import Asia from './Asia'

function sortTableMuertes(m, a, r, c) {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("tabla");
  switching = true;

  if(m.classList.contains("bi-filter-square")){
    m.classList.remove("bi-filter-square");
    m.classList.toggle("bi-filter-square-fill");
    if(a.classList.contains("bi-filter-square-fill")){
      a.classList.remove("bi-filter-square-fill");
      a.classList.toggle("bi-filter-square");
    }
    if(c.classList.contains("bi-filter-square-fill")){
      c.classList.remove("bi-filter-square-fill");
      c.classList.toggle("bi-filter-square");
    }
    if(r.classList.contains("bi-filter-square-fill")){
      r.classList.remove("bi-filter-square-fill");
      r.classList.toggle("bi-filter-square");
    }
  }   

  while (switching) {
    switching = false;
    rows = table.rows;

    for (i = 1; i < rows.length - 1; i++) {
      shouldSwitch = false;

      x = rows[i].getElementsByTagName("TD")[3];
      y = rows[i + 1].getElementsByTagName("TD")[3];

      if (Number(x.innerHTML) < Number(y.innerHTML)) {
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}

function sortTableActivos(m, a, r, c) {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("tabla");
  switching = true;

  if(a.classList.contains("bi-filter-square")){
    a.classList.remove("bi-filter-square");
    a.classList.toggle("bi-filter-square-fill");   
    if(r.classList.contains("bi-filter-square-fill")){
      r.classList.remove("bi-filter-square-fill");
      r.classList.toggle("bi-filter-square");
    }
    if(c.classList.contains("bi-filter-square-fill")){
      c.classList.remove("bi-filter-square-fill");
      c.classList.toggle("bi-filter-square");
    }
    if(m.classList.contains("bi-filter-square-fill")){
      m.classList.remove("bi-filter-square-fill");
      m.classList.toggle("bi-filter-square");
    }
  } 

  while (switching) {
    switching = false;
    rows = table.rows;

    for (i = 1; i < rows.length - 1; i++) {
      shouldSwitch = false;

      x = rows[i].getElementsByTagName("TD")[4];
      y = rows[i + 1].getElementsByTagName("TD")[4];

      if (Number(x.innerHTML) < Number(y.innerHTML)) {
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}

function sortTableConfirmados(m, a, r, c) {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("tabla");
  switching = true;

  if(c.classList.contains("bi-filter-square")){
    c.classList.remove("bi-filter-square");
    c.classList.toggle("bi-filter-square-fill"); 
    if(a.classList.contains("bi-filter-square-fill")){
      a.classList.remove("bi-filter-square-fill");
      a.classList.toggle("bi-filter-square");
    }
    if(r.classList.contains("bi-filter-square-fill")){
      r.classList.remove("bi-filter-square-fill");
      r.classList.toggle("bi-filter-square");
    }
    if(m.classList.contains("bi-filter-square-fill")){
      m.classList.remove("bi-filter-square-fill");
      m.classList.toggle("bi-filter-square");
    }
  }

  while (switching) {
    switching = false;
    rows = table.rows;

    for (i = 1; i < rows.length - 1; i++) {
      shouldSwitch = false;

      x = rows[i].getElementsByTagName("TD")[1];
      y = rows[i + 1].getElementsByTagName("TD")[1];

      if (Number(x.innerHTML) < Number(y.innerHTML)) {
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}

function sortTableRecuperados(m, a, r, c) {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("tabla");
  switching = true;

  if(r.classList.contains("bi-filter-square")){
  r.classList.remove("bi-filter-square");
  r.classList.toggle("bi-filter-square-fill");
    if(a.classList.contains("bi-filter-square-fill")){
      a.classList.remove("bi-filter-square-fill");
      a.classList.toggle("bi-filter-square");
    }
    if(c.classList.contains("bi-filter-square-fill")){
      c.classList.remove("bi-filter-square-fill");
      c.classList.toggle("bi-filter-square");
    }
    if(m.classList.contains("bi-filter-square-fill")){
      m.classList.remove("bi-filter-square-fill");
      m.classList.toggle("bi-filter-square");
    }
  }

  while (switching) {
    switching = false;
    rows = table.rows;

    for (i = 1; i < rows.length - 1; i++) {
      shouldSwitch = false;

      x = rows[i].getElementsByTagName("TD")[2];
      y = rows[i + 1].getElementsByTagName("TD")[2];

      if (Number(x.innerHTML) < Number(y.innerHTML)) {
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}

const Tabla = () => {

  const [estado, setEstado] = useState([]);

  useEffect(() => {
    obtenerDatos();
  }, []);

  const obtenerDatos = async () => {
    const datos = await fetch("https://covid-193.p.rapidapi.com/statistics", {
      method: "GET",
      headers: {
        "x-rapidapi-key": "2344368433msh7a2c633c4049484p189ed7jsn67fcf84f1276",
        "x-rapidapi-host": "covid-193.p.rapidapi.com",
      },
    });
    const json = await datos.json();
    var a = json.response;

    const datosFinal = a.filter(
      (b) =>
        b.country !== "All" &&
        b.country !== "Asia" &&
        b.country !== "Europe" &&
        b.country !== "North-America" &&
        b.country !== "South-America"
    );
    setEstado(datosFinal);
  };

  const divStyle={
    overflowY: 'scroll',
    border:'1px solid ',
    width:'100%',
    float: 'left',
    height:'500px',
    position:'relative'
  };

    var m = document.getElementById("muertes");
    var r = document.getElementById("recuperados");
    var a = document.getElementById("activos");
    var c = document.getElementById("confirmados");
  return (
<div>
  <div style={divStyle}>
    <table id="tabla" className="table table-striped table-sm">
      <thead>
        <tr>
          <th scope="col">País</th>
          <th
            scope="col"
            onClick={() => {
              sortTableConfirmados(m, a, r, c);
            }}
          >
            Confirmados
          <i id="confirmados" className="bi bi-filter-square"></i>
          </th>
          <th
            scope="col"
            onClick={() => {
              sortTableRecuperados(m, a, r, c);
            }}
          >
            Recuperados
          <i id="recuperados" className="bi bi-filter-square"></i>
          </th>
          <th
            scope="col"
            onClick={() => {
              sortTableMuertes(m, a, r, c);
            }}
          >
            Muertes
          <i id="muertes" className="bi bi-filter-square"></i>
          </th>
          <th
            scope="col"
            onClick={() => {
              sortTableActivos(m, a, r, c);
            }}
          >
            Nuevos
          <i id="activos" className="bi bi-filter-square"></i>
          </th>
        </tr>
      </thead>
      <tbody>
        {estado.map((item) => (
          <tr>
            <td>{item.country}</td>
            <td>{item.cases.total}</td>
            <td>{item.cases.recovered}</td>
            <td>{item.deaths.total}</td>
            <td>{item.cases.new}</td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
  <div className="accordion" id="accordionFlushExample">
    <AmericaNorte/>
    <AmericaSur/>
    <Asia/>
    <Europa/>
  </div>
</div>
  );
};

export default Tabla;