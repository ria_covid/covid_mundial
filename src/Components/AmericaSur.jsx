import React, { useState, useEffect } from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-icons/font/bootstrap-icons.css";
import cov19 from "../multimedia/cov19_2.png"

const AmericaSur = () =>{
    const [estado4, setEstado4] = useState([]);

    useEffect(() => {
      obtenerDatos();
    }, []);
  
    const obtenerDatos = async () => {
      const datos = await fetch("https://covid-193.p.rapidapi.com/statistics", {
        method: "GET",
        headers: {
          "x-rapidapi-key": "2344368433msh7a2c633c4049484p189ed7jsn67fcf84f1276",
          "x-rapidapi-host": "covid-193.p.rapidapi.com",
        },
      });

      const json = await datos.json();
      var datos_mundo;
      var datos_grales = json.response;
        datos_grales.forEach(dato => {
            if(dato.country=="South-America"){
                datos_mundo= dato;
                setEstado4(datos_mundo);
            }
        });
    };

    if(estado4.cases!= undefined ){
        var total = estado4.cases["total"];
        var recuperado = estado4.cases["recovered"];
        var criticos = estado4.cases["critical"];
        var muertes = estado4.deaths["total"];
    }
 
  return (
    <div className="accordion-item fondo_transparente">
      <button style={{backgroundColor: 'transparent'}} className="accordion-button " type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="true" aria-controls="panelsStayOpen-collapseTwo">          
        <img style={{ width: '120px', height: '100%'}} src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0f/South_America_%28orthographic_projection%29.svg/400px-South_America_%28orthographic_projection%29.svg.png"/>     
        <h2 className="accordion-header m-2" id="panelsStayOpen-headingTwo"> America del Sur</h2>  
      </button>
      <div id="panelsStayOpen-collapseTwo" className="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo" data-bs-parent="#accordionFlushExample" style={{ backgroundImage: `url(${cov19})`, backgroundRepeat:'no-repeat', backgroundPosition: 'center', backgroundSize: 'cover'}}>
      <br/>
      <div className="container">
        <div className="row row-40">
          <div className="col-sm-6 col-md-3">
            <div className="box-counter box-counter-inverse"><span className="novi-icon icon icon-lg icon-primary mercury-icon-group"></span>
              <p className="box-header"><b>Confirmados</b></p>
              <div className="text-large counter"> {total}</div>
            </div>
          </div>
          <div className="col-sm-6 col-md-3">
            <div className="box-counter box-counter-inverse"><span className="novi-icon icon icon-lg-smaller icon-primary mercury-icon-scales"></span>
              <p className="box-header"><b>Recuperados</b></p>
              <div className="text-large counter">{recuperado}</div>
            </div>
          </div>
          <div className="col-sm-6 col-md-3">
            <div className="box-counter box-counter-inverse"><span className="novi-icon icon icon-lg-smaller icon-primary mercury-icon-partners"></span>
              <p className="box-header"><b>Críticos</b></p>
              <div className="text-large counter ">{criticos}</div>
            </div>
          </div>
          <div className="col-sm-6 col-md-3">
            <div className="box-counter box-counter-inverse"><span className="novi-icon icon icon-lg icon-primary mercury-icon-case"></span>
              <p className="box-header"><b>Muertes</b></p>
              <div className="text-large counter">{muertes}</div>
            </div>
          </div>
        </div>
      </div>
      <br/>
      </div>
    </div>
  );
};

export default AmericaSur;