import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-icons/font/bootstrap-icons.css";
import "../Styles/Estilos.css";
import{
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Totales from './Totales'
import Comparacion from './Comparacion'
import Informacion from './Informacion'
import DatosPais from './DatosPais';
import Tabla from './Tabla';
import covid from "../multimedia/covid.png";
import {getJSONData, countries} from "../funciones"
export var countryselect;

function Navbar(){
  var paises = {};

  window.onload = function(){
      countryselect="Uruguay";
        var st_modo = localStorage.getItem("darkMode");
        var check=document.getElementById("idchk");

          if(st_modo==="on"){
            dk_lg_mode();
            check.checked = true;
          }else if(st_modo==="off"){
            day_lg_mode();
            check.checked = false;
          }
          
          for(let i=1; i<6; i++){
            var modal = document.getElementById("myModal"+i);
           
             var img = document.getElementById("myImg"+i);
             var modalImg = document.getElementById("img"+i);
             var captionText = document.getElementById("caption"+i);
             img.onclick = function(){
               modal.style.display = "block";
               modalImg.src = this.src;
               captionText.innerHTML = this.alt;
             }
             var span = document.getElementsByClassName("close")[i-1];
             span.onclick = function() { 
               modal.style.display = "none";
             }
          }
  };

getJSONData(countries).then(function(resultObj){
  if (resultObj.status === "ok"){

      paises = resultObj.data;
      const  nombres = paises.response;
 
        var options;
        for (let i = 0; i < nombres.length; i++) {
              options += '<option value="' + nombres[i] + '" label="' + nombres[i] + '" />';
        }
        var buscador = document.getElementById("mySearch");
        var busca = document.getElementById("submit");
        var acomparar = document.getElementById("aComparar");
        var lista = document.getElementById('datalistPaises');
        if(lista!=null){
          lista.innerHTML = options;
        
          busca.onclick = function(){
            var texto = buscador.value;
            let paises = [];
            for(let j = 0; j < nombres.length; j++){
              paises.push(nombres[j]);
            }
              if(!paises.includes(texto)){
                window.alert("Ese país no pertenece!");
              }else{
                countryselect = texto;
                localStorage.setItem("paisseleccionado", countryselect);
                buscador.setAttribute("value", "");
              }
          }
          acomparar.onclick = function(){
            localStorage.setItem("paisseleccionado", "");
          }
                 
        }
      }
});

function validar_dk_mode(){
  var v_body = document.body;
    if(!v_body.classList.contains("bg_night")){
      localStorage.setItem("darkMode", "on");
      dk_lg_mode();
    }else{
      localStorage.setItem("darkMode", "off" );
      day_lg_mode();
    }
};

function dk_lg_mode(){
  var v_body = document.body;
  var v_barra = document.getElementById("barra_nav");
  var v_elemento = document.getElementById("tabla"); 
  if(v_elemento!=null){
          v_elemento.classList.toggle("table-dark");
  }
      v_barra.classList.remove("bg-light");
      v_body.classList.toggle("bg_night");
      v_barra.classList.toggle("bg-dark");
};
function day_lg_mode(){
  var v_body = document.body;
  var v_barra = document.getElementById("barra_nav");
  var v_elemento = document.getElementById("tabla"); 
  if(v_elemento!=null){
      v_elemento.classList.remove("table-dark");
  }
      v_barra.classList.toggle("bg-light");
      v_body.classList.remove("bg_night");
      v_barra.classList.remove("bg-dark");
};

  return(
    <div className="Nav_Boot ">
      <Router>
      <nav className="navbar navbar-expand-lg navbar-light bg-light row" id="barra_nav">
        <div className="container">
          <div className="row align-items-start">
            <div className="col">          
              <img srcSet={covid} width="40" height="40" />
            </div>
            <div className="col">
              <form className="d-flex">
                    <input list="datalistPaises" className="me-2" id="mySearch" placeholder="País.." data-bs-toggle="tooltip" data-bs-placement="top" title="Seleccione un país."/>
                    <Link to="/datospais">
                      <button className="btn btn-outline-success bi bi-search" onClick={'window.location.reload()'} id="submit" type="button" data-bs-toggle="tooltip" data-bs-placement="top" title="Obtener datos del país."></button>
                    </Link>
                    <datalist id="datalistPaises"></datalist>
              </form>
            </div>
          </div>
            <button className="navbar-toggler btn" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
          <div className="row align-items-center">
            <div className="col"> 
              <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <li className="nav-item">
                    <Link to="/informacion">
                      <button className="btn btn-outline-success me-2" type="button" data-bs-toggle="tooltip" data-bs-placement="top" title="Información OMS.">Información sobre Covid-19</button>
                    </Link>
                  </li>
                  <li className="nav-item" id="li_tabla">
                    <Link to="/">           
                      <button className="btn btn-outline-success me-2" onClick={'window.location.reload()'} type="button" data-bs-toggle="tooltip" data-bs-placement="top" title="Tabla informativa.">Tabla</button>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/comparacion">
                      <button id="aComparar" className="btn btn-outline-success me-2" type="button"  data-bs-toggle="tooltip" data-bs-placement="top" title="Comparar dos países.">Comparación</button>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>          
          <div className="justify-content-end">     
              <label className="switch" data-bs-toggle="tooltip" data-bs-placement="top" title="Cambiar Modo">
                <input type="checkbox" id="idchk"/>
                <span className="slider round" onClick={validar_dk_mode} ></span>
              </label>
          </div>        
        </div>
      </nav>    
        <Totales/>
        <Switch>   
          <Route path="/datospais" extact>
            <DatosPais/>
          </Route>
          <Route path="/comparacion" extact>
            <Comparacion/>
          </Route>
          <Route path="/informacion" extact>
            <Informacion/>
          </Route>
          <Route path="/" extact>
            <Tabla/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}
  
export default Navbar;