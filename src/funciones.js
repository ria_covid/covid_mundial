export const countries = "https://covid-193.p.rapidapi.com/countries";

export function fechaFormato(fecha, formato) {
  var date = fecha;
  var dd = date.getDate();
  if (dd < 10) dd = "0" + dd.toString();
  else dd = dd.toString();

  var mm = date.getMonth() + 1;
  if (mm < 10) mm = "0" + mm.toString();
  else mm = mm.toString();

  var HH = date.getHours();
  if (HH < 10) HH = "0" + HH.toString();
  else HH = HH.toString();

  var mms = date.getMinutes();
  if (mms < 10) mms = "0" + mms.toString();
  else mms = mms.toString();

  var yyyy = date.getFullYear();
  if (formato === 0) return yyyy + "-" + mm + "-" + dd;
  else return yyyy + "-" + mm + "-" + dd + " " + "23" + ":" + "59" + ":00";
}

export function fechaHoraActual(formato) {
  var date = new Date();
  var dd = date.getDate();
  if (dd < 10) dd = "0" + dd.toString();
  else dd = dd.toString();

  var mm = date.getMonth() + 1;
  if (mm < 10) mm = "0" + mm.toString();
  else mm = mm.toString();

  var HH = date.getHours();
  if (HH < 10) HH = "0" + HH.toString();
  else HH = HH.toString();

  var mms = date.getMinutes();
  if (mms < 10) mms = "0" + mms.toString();
  else mms = mms.toString();

  var yyyy = date.getFullYear();
  switch (formato) {
    case 1:
      return yyyy + "-" + mm + "-" + dd;
      break;
    case 2:
      return yyyy + "-" + mm + "-" + dd + " " + HH + ":" + mms + ":00";
      break;
    default:
      return yyyy + "-" + mm + "-" + dd + " " + HH + ":" + mms + ":00";
      break;
  }
}

export async function getJSONData(url){
    var result = {};
    return fetch(url, {
        "method": "GET",
        "body": JSON.stringify(),
        "headers": {
            "x-rapidapi-key": "2344368433msh7a2c633c4049484p189ed7jsn67fcf84f1276",
            "x-rapidapi-host": "covid-193.p.rapidapi.com"
        }
    })
    .then(response => {
      if (response.ok) {
        return response.json();
      }else{
        throw Error(response.statusText);
      }
    })
    .then(function(response) {
          result.status = 'ok';
          result.data =  response;
          return result;
    })
    .catch(function(error) {
        result.status = 'error';
        result.data = error;
        return result;
    });
  }