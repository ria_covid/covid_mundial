import React, { useState, useEffect } from 'react';
import { Bar }  from  'react-chartjs-2' ; 
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-icons/font/bootstrap-icons.css";
import "chart.js"
import {fechaFormato} from "../funciones"

const Grafica1 = () =>{

  const [estado, setEstado] = useState([]);
    var fechas = [];  
    var urls = [];
    if(localStorage.getItem("paisseleccionado")!=null){
  var pais = localStorage.getItem("paisseleccionado");
    }else{
  var pais = "";
    }
  lagrafica(pais);

  var buscar=document.getElementById("submit");
if(buscar!=undefined){
  buscar.addEventListener("click", function(){
      
    pais = localStorage.getItem("paisseleccionado");
    lagrafica(pais);
    window.location.reload();
  });
}

function lagrafica(pais){
  var fecha = new Date();
  fecha.setDate(fecha.getDate() - 7);

  for (let i = 0; i < 7; i++) {
    fecha.setDate(fecha.getDate() + 1);
    fechas.push(fechaFormato(fecha, 0));
  }
    fechas.forEach((elemento) => {
      var url =
        "https://covid-193.p.rapidapi.com/history?country=" +
        pais +
        "&day=" +
        elemento;
      urls.push(url);
    });
}
 
  var casosNuevos = [];
  var fechasGrafica = [];
  var objetoSesion = [];
  useEffect(() => {
    obtenerDatos(urls[0], fechas[0]);
    obtenerDatos(urls[1], fechas[1]);
    obtenerDatos(urls[2], fechas[2]);
    obtenerDatos(urls[3], fechas[3]);
    obtenerDatos(urls[4], fechas[4]);
    obtenerDatos(urls[5], fechas[5]);
    obtenerDatos(urls[6], fechas[6]);
  }, []);

  const obtenerDatos = async (url, fecha) => {
    const datos = await fetch(url, {
      method: "GET",
      headers: {
        "x-rapidapi-key": "2344368433msh7a2c633c4049484p189ed7jsn67fcf84f1276",
        "x-rapidapi-host": "covid-193.p.rapidapi.com",
      },
    });
    const json = await datos.json();
    var a = json.response;
  if(a!=undefined&&a[0]!=undefined){
    objetoSesion[fecha]=a[0].cases.new;   
    casosNuevos.push(a[0].cases.new);
  }
    fechasGrafica.push(fecha);

    setEstado([casosNuevos, fechasGrafica]);
    sessionStorage.setItem("datos", casosNuevos);
    sessionStorage.setItem("fechas", fechasGrafica);
  };
  
  Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
      if (this[i] == deleteValue) {         
        this.splice(i, 1);
        i--;
      }
    }
    return this;
  };
  

if(estado[0]!=(undefined)){
  var datas = [{
    x: estado[1][0],
    y: estado[0][0]
  },
  {
    x: estado[1][1],
    y: estado[0][1]
  },
  {
    x: estado[1][2],
    y: estado[0][2]
  },
  {
    x: estado[1][3],
    y: estado[0][3]
  },
  {
    x: estado[1][4],
    y: estado[0][4]
  },
  {
    x: estado[1][5],
    y: estado[0][5]
  },
  {
    x: estado[1][6],
    y: estado[0][6]
  }];
  var myJSON = JSON.stringify(datas);
  sessionStorage.setItem("Data", myJSON);
}

return(
    <div>
      <Bar id="grafic"
        data={{
            labels: fechas,
            datasets: [{
                label: 'Casos nuevos de '+pais,
                data: datas,
                backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(255, 0, 255, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)',
                  'rgba(0, 255, 0, 0.2)',
                ],
                borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(255, 0, 255, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)',
                  'rgba(0, 255, 0, 1)',
                ],
                borderWidth: 2,
                tension: 0.1,
              },
            ],
          }}
          height={300}
          width={300}
          options={{
            responsive : true,
            maintainAspectRatio: false,
            scales: {
              xAxes: [{
                display: true,
                scaleLabel: {
                  display: true,
                  labelString: 'x axis label'
                }
              }],
              yAxes: [{
                display: true,
                scaleLabel: {
                  display: true,
                  labelString: 'y axis label'
                }
              }]
            },
          }}
        />
    </div>
    );
}

export default Grafica1;