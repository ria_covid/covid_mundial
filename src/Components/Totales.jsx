import React, { useState, useEffect } from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-icons/font/bootstrap-icons.css";
import covid19 from "../multimedia/covid19.png";

const Totales = () =>{
    const [estado2, setEstado2] = useState([]);

    useEffect(() => {
      obtenerDatos();
    }, []);
  
    const obtenerDatos = async () => {
      const datos = await fetch("https://covid-193.p.rapidapi.com/statistics", {
        method: "GET",
        headers: {
          "x-rapidapi-key": "2344368433msh7a2c633c4049484p189ed7jsn67fcf84f1276",
          "x-rapidapi-host": "covid-193.p.rapidapi.com",
        },
      });

      const json = await datos.json();
      var datos_mundo;
      var datos_grales = json.response;
        datos_grales.forEach(dato => {
            if(dato.country=="All"){
                datos_mundo= dato;
                setEstado2(datos_mundo);
            }
        });
    };

    if(estado2.cases!= undefined ){
        var total = estado2.cases["total"];
        var recuperado = estado2.cases["recovered"];
        var criticos = estado2.cases["critical"];
        var muertes = estado2.deaths["total"];
    }
 
    return (
        <div id="totales" className="row parallax-content" style={{backgroundImage: 'url("https://wallpaperaccess.com/full/3816347.png")', backgroundRepeat  : 'no-repeat', backgroundPosition: 'center', backgroundSize: 'cover'}} data-parallax-img="images/progress-bars-parallax-1.jpg">
            <div className="col col-md-auto">
                  <div className="box-counter ">
                    <p className="box-header"><b>Covid-19 en el Mundo</b></p>
                    <div className="counter"><img src={covid19} width="120"></img></div>
                  </div>
            </div>
            <div className="container col">
              <div className="row row-40 py-5 row-cols-1 row-cols-md-3">  
                <div className="feature col-sm-6 col-lg-3">
                  <div className="box-counter ">
                    <p className="box-header"><b>Confirmados</b></p>
                    <div className="text-large counter"> {total}</div>
                  </div>
                </div>
                <div className="feature col-sm-6 col-lg-3">
                  <div className="box-counter ">
                    <p className="box-header"><b>Recuperados</b></p>
                    <div className="text-large counter">{recuperado}</div>
                  </div>
                </div>
                <div className="feature col-sm-6 col-lg-3">
                  <div className="box-counter ">
                    <p className="box-header"><b>Críticos</b></p>
                    <div className="text-large counter">{criticos}</div>
                  </div>
                </div>
                <div className="feature col-sm-6 col-lg-3">
                  <div className="box-counter ">
                    <p className="box-header"><b>Muertes</b></p>
                    <div className="text-large counter">{muertes}</div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    );
};
export default Totales;


