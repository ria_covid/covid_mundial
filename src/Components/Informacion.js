import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-icons/font/bootstrap-icons.css";

const Informacion = () =>{

    return (
        <div className="container">
            <br/>
            <div className="row" >
                <div className="responsive-video" >
                    <iframe className="video" src="https://www.youtube.com/embed/Ug2MD9Wv3oY" title="OMS INFORMATION" allowFullScreen></iframe>
                </div>
            </div>
            <div className="row justify-content-evenly">
                <div className="col col-sm-auto m-2 ">
                    <a  href="https://www.who.int/es" data-bs-toggle="tooltip" data-bs-placement="top" title="Página Oficial OMS."> 
                        <img src="https://sileu.com/wp-content/uploads/2020/03/OMS-1.png" height="250"></img>
                    </a>
                </div>
                <div className="col col-sm-auto m-2 align-self-end">
                    <a href="https://www.who.int/es/news-room/q-a-detail"> 
                        <img className="counter" src="https://www.nicepng.com/png/full/239-2394605_preguntas-png-ponto-de-interrogao-png.png" height="220"  data-bs-toggle="tooltip" data-bs-placement="top" title="Preguntas y respuestas sobre COVID-19."></img>
                    </a>
                </div>
            </div>
        </div>
    );
};

export default Informacion;