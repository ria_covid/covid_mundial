import React, { useState, useEffect } from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-icons/font/bootstrap-icons.css";
import Grafica from './Grafica';

const DatosPais = () =>{
    const [estado7, setestado7] = useState([]);

    var pais = localStorage.getItem("paisseleccionado");
    useEffect(() => {
      obtenerDatos();
    }, []); 
  
    const obtenerDatos = async () => {
      const datos = await fetch("https://covid-193.p.rapidapi.com/statistics", {
        method: "GET",
        headers: {
          "x-rapidapi-key": "2344368433msh7a2c633c4049484p189ed7jsn67fcf84f1276",
          "x-rapidapi-host": "covid-193.p.rapidapi.com",
        },
      });

      const json = await datos.json();
      var datos_pais;
      var datos_grales = json.response;
      if(datos_grales!=undefined){
        datos_grales.forEach(dato => {
            if(dato.country==pais){
                datos_pais= dato;
                console.log(datos_pais.tests["total"]);
                setestado7(datos_pais);
            }
        });
      }
    };

    if(estado7.cases!= undefined ){
        var total = estado7.cases["total"];
        var recuperado = estado7.cases["recovered"];
        var criticos = estado7.cases["critical"];
        var muertes = estado7.deaths["total"];
        var muertes_n =estado7.deaths["new"];
        var active = estado7.cases["active"];
        var nuevos = estado7.cases["new"];
        var test = estado7.tests["total"];
    }

    if(estado7.day!=undefined){
        var day = estado7.day;
    }else{
        var day = "";
    }
 
    return (
        <div>
            <h2 className="text-center">{pais}</h2>
            <h2 className="text-center">{" Fecha: "+ day}</h2> 
            <div className="row row-cols-1 row-cols-md-3 g-4">
                <div className="col">
                    <div className="card fondo_transparente text-center">
                    <div className="text-center counter">
                    <img src="https://image.flaticon.com/icons/png/512/2766/2766095.png" style={{width:125, textAlign: "center"}} className="card-img-top" alt="Casos"/>
                    </div>
                        <div className="card-body">
                            <h5 className="card-title">Casos</h5>
                            <p className="card-text"> 
                                                    Activos: {active} <br/> 
                                                    Críticos: {criticos} <br/>
                                                    Nuevos: {nuevos} <br/>
                                                    Recuperados: {recuperado} <br/>
                                                    Total: {total} <br/>
                                                    Tests Realizados: {test} 
                            </p>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card fondo_transparente text-center">
                    <div className="text-center counter"><img src="https://image.flaticon.com/icons/png/512/2927/2927432.png" style={{width:125, textAlign: "center"}} className="card-img-top" alt="Muertes"/></div> 
                        <div className="card-body">
                            <h5 className="card-title">Muertes</h5>
                            <p className="card-text">
                                                    Nuevas: {muertes_n} <br/>
                                                    Total: {muertes} 
                            </p>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card fondo_transparente text-center">
                    <div className="text-center counter"><img src="https://image.flaticon.com/icons/png/512/2947/2947656.png" style={{width:125}} className="card-img-top" alt="Información del País"/></div>                        
                        <div className="card-body">
                            <h5 className="card-title">Datos Generales</h5>
                            <p className="card-text">
                                                    Continente: {estado7.continent} <br/>
                                                    Población: {estado7.population} <br/>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <Grafica/>
        </div>
    );
};

export default DatosPais;