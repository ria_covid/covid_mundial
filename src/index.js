import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Components/App';
import Navbar from './Components/Navbar'
import Footer from './Components/Footer'

ReactDOM.render(
  <React.StrictMode>
    <Navbar/>
    <App/>
    <Footer/>
  </React.StrictMode>,
  document.getElementById('root')
);