import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-icons/font/bootstrap-icons.css";

function Footer(){

  return(
    <footer className="text-center " style={{'backgroundColor': 'rgba(0, 0, 0, 0.2)'}}>
      <hr></hr>
        <div className="container p-4">
          <div className="row">
            <div className="col-sm">             
              <div className="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
                <img id="myImg1" src="https://www.who.int/images/default-source/health-topics/coronavirus/person-sick-in-your-household-what-to-do-es.png" className="w-100"/>
                <div id="myModal" className="modal">
                  <span className="close">&times;</span>
                  <img className="modal-content" id="img1"/>
                  <div id="caption"></div>
                </div>
              </div>
            </div>
            <div className="col-sm">
              <div className="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
                <img id="myImg2" src="https://www.who.int/images/default-source/health-topics/coronavirus/person-sick-in-your-household-prepare-es.jpg" className="w-100"/>
                <div id="myModal2" className="modal">
                  <span className="close">&times;</span>
                  <img className="modal-content" id="img2"/>
                  <div id="caption2"></div>
                </div>
              </div>
            </div>
            <div className="col-sm">
              <div className="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
                <img
                  id="myImg3" src="https://www.who.int/images/default-source/health-topics/coronavirus/dont-put-off-medical-appointments-es.jpg" className="w-100"/>
                <div id="myModal3" className="modal">
                  <span className="close">&times;</span>
                  <img className="modal-content" id="img3"/>
                  <div id="caption3"></div>
                </div>
              </div>
            </div>
            <div className="col-sm">
              <div className="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
                <img id="myImg4" src="https://www.who.int/images/default-source/health-topics/coronavirus/visiting-family-es.jpg" className="w-100"/>
                <div id="myModal4" className="modal">
                  <span className="close">&times;</span>
                  <img className="modal-content" id="img4"/>
                  <div id="caption4"></div>
                </div>
              </div>
            </div>
            <div className="col-sm">
              <div className="bg-image hover-overlay ripple shadow-1-strong rounded" data-ripple-color="light">
                <img id="myImg5" src="https://www.who.int/images/default-source/health-topics/coronavirus/grocery-shopping-es.jpg" className="w-100"/>
                <div id="myModal5" className="modal">
                  <span className="close">&times;</span>
                  <img className="modal-content" id="img5"/>
                  <div id="caption5"></div>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div className="text-center p-3" style={{'backgroundColor': 'rgba(0, 0, 0, 0.2)'}}>
        © 2021 Rafael Olivera & Joseline Rodríguez
      </div>
    </footer>
  );
}

export default Footer;