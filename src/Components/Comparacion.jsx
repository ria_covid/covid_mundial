import React, { useState, useEffect } from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-icons/font/bootstrap-icons.css";
import "chart.js"
import {getJSONData, countries} from "../funciones"
import DatosPais from './DatosPais';

const Comparacion = () =>{
    var paises = {};
    const [estadoPais1, setEstadoPais1] = useState([]);
    const [estadoPais2, setEstadoPais2] = useState([]);

    getJSONData(countries).then(function(resultObj){
        if (resultObj.status === "ok"){
      
            paises = resultObj.data;
            const  nombres = paises.response;
       
              var options;
              for (let i = 0; i < nombres.length; i++) {
                    options += '<option value="' + nombres[i] + '" label="' + nombres[i] + '" />';
              }
              var buscador1 = document.getElementById("compara1");
              var buscador2 = document.getElementById("compara2");
              var busca1 = document.getElementById("submit1");
              var busca2 = document.getElementById("submit2");
              var lista1 = document.getElementById('datalistPaises1');
              var lista2 = document.getElementById('datalistPaises2');

            if(lista1!=null){
                lista1.innerHTML = options;
                lista2.innerHTML = options;

                busca1.onclick = function(){
                  var texto1 = buscador1.value;
                  let paises1 = [];
                  for(let j = 0; j < nombres.length; j++){
                    paises1.push(nombres[j]);
                  }
                    if(!paises1.includes(texto1)){
                      window.alert("Ingrese un país válido");
                    }else{
                      var country1 = texto1;
                      localStorage.setItem("paisseleccionado", country1);
                      setEstadoPais1(<DatosPais/>);
                    }
                }
                busca2.onclick = function(){
                  var texto2 = buscador2.value;
                  let paises2 = [];
                  for(let j = 0; j < nombres.length; j++){
                    paises2.push(nombres[j]);
                  }
                    if(!paises2.includes(texto2)){
                      window.alert("Ingrese un país válido");
                    }else{
                      var country2 = texto2;
                      localStorage.setItem("paisseleccionado", country2);
                      setEstadoPais2(<DatosPais/>);
                    }
                }
            }
        }
    });  

  return(
    <div className="container">
    <br/>
      <div className="row" id="comparaID">
        <div className="col">
                <div className="d-flex justify-content-center">
                  <form className="d-flex">
                        <input list="datalistPaises1" className="me-2" id="compara1" placeholder="Primer País.." data-bs-toggle="tooltip" data-bs-placement="top" title="Seleccione un país."/>
                          <button className="btn btn-outline-success bi bi-search" id="submit1" type="button" data-bs-toggle="tooltip" data-bs-placement="top" title="Obtener datos del país."></button>
                        <datalist id="datalistPaises1"></datalist>
                  </form>
                </div>
                <div>{estadoPais1}</div>
        </div>
        <div className="col">
                <div className="d-flex justify-content-center">
                  <form className="d-flex">
                        <input list="datalistPaises2" className="me-2" id="compara2" placeholder="Segundo País.." data-bs-toggle="tooltip" data-bs-placement="top" title="Seleccione un país."/>
                          <button className="btn btn-outline-success bi bi-search" id="submit2" type="button" data-bs-toggle="tooltip" data-bs-placement="top" title="Obtener datos del país."></button>
                        <datalist id="datalistPaises2"></datalist>
                  </form>
                </div>
                <div>{estadoPais2}</div>
        </div>
      </div>
    </div>
  );
 }

export default Comparacion;